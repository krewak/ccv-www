<?php

namespace CCV\Helpers;

use PHPMailer\PHPMailer\PHPMailer;

class EmailSender {

	public function send(array $request): void {
		$mail = new PHPMailer(true);

		$mail->isSMTP();
		$mail->Host = "smtp.googlemail.com";
		$mail->SMTPAuth = true;
		$mail->Username = getenv("SMTP_EMAIL_ADDRESS");
		$mail->Password = getenv("SMTP_EMAIL_PASSWORD"); 
		$mail->SMTPSecure = "ssl";
		$mail->Port = 465;
		$mail->CharSet = "UTF-8";

		$mail->SMTPOptions = [
			"ssl" => [
				"verify_peer" => false,
				"verify_peer_name" => false,
				"allow_self_signed" => true
			]
		];

		$subject = "Nowe zgłoszenie na konferencję CCV: " . $request["name"];
		$datetime = date("Y/m/d H:i:s");

		$body = "Nowe zgłoszenie na konferencję CCV: <br><br>";
		$body .= "Nazwisko: <strong>" . $request["name"] . "</strong><br>";
		$body .= "Afiliacja: <strong>" . $request["affiliation"] . "</strong><br>";
		$body .= "Adres email: <strong>" . $request["email"] . "</strong><br>";
		$body .= "Rezerwacja: <strong>" . $request["reservation"] . "</strong><br>";
		$body .= "Dodatkowe informacje: <strong>" . $request["additional"] . "</strong><br>";
		$body .= "Nadesłano: <strong>" . $datetime . "</strong>";

		$mail->setFrom(getenv("SMTP_EMAIL_ADDRESS"), "CCV form");
		$mail->addAddress(getenv("CONTACT_EMAIL"), $request["name"]);
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $body;

		$mail->send();
	}

}
