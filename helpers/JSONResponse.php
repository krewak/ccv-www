<?php

namespace CCV\Helpers;

use JsonSerializable;

class JSONResponse implements JsonSerializable {

	private $message = "";
	private $errors = [];

	public function jsonSerialize(): array {
		return get_object_vars($this);
	}

	public function setMessage(string $message): self {
		$this->message = $message;
		return $this;
	}

	public function setErrors(array $errors): self {
		$this->errors = $errors;
		return $this;
	}

}
