<?php

namespace CCV;

use CCV\Helpers\EmailSender;
use CCV\Helpers\JSONResponse;
use Exception;

class EmailController extends Controller {

	private $emailer;
	private $request;
	private $response;

	public function __construct(array $request) {
		$this->request = $request;
		$this->response = new JSONResponse();
		$this->emailer = new EmailSender();

		$this->setHeader("application/json");
		$this->setHttpCode(400);
	}

	public function render(): void {
		$this->sanitizeRequest();
		$validationErrors = $this->validateRequest();

		if(count($validationErrors)) {
			$this->response->setErrors($validationErrors);
		} else {
			$this->emailer->send($this->request);
			$this->setHttpCode(200);
		}

		$this->returnJSON();
	}

	private function sanitizeRequest() {
		foreach($this->request as $data) {
			$data = strip_tags(trim($data));
		}

		if(!isset($this->request["additional"])) {
			$this->request["additional"] = "";
		}
	}

	private function validateRequest(): array {
		$errors = [];

		if(!isset($this->request["email"]) || !$this->request["email"]) {
			$errors["email"] = "is required.";
		} elseif(!filter_var($this->request["email"], FILTER_VALIDATE_EMAIL)) {
			$errors["email"] = "must have email form.";
		}

		if(!isset($this->request["name"]) || !$this->request["name"]) {
			$errors["name"] = "is required.";
		}

		if(!isset($this->request["affiliation"]) || !$this->request["affiliation"]) {
			$errors["affiliation"] = "is required.";
		}

		if(!isset($this->request["reservation"]) || !$this->request["reservation"]) {
			$errors["reservation"] = "Selection is required.";
		}

		return $errors;
	}

	private function returnJSON(): void {
		echo json_encode($this->response);
		die();
	}

}
