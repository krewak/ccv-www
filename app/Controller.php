<?php

namespace CCV;

abstract class Controller {

	abstract public function render(): void;

	public function setHeader(string $contentType): void {
		header("Content-type: " . $contentType);
	}

	public function setHttpCode(int $code): void {
		http_response_code($code);
	}

}
