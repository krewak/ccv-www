<?php

namespace CCV;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

class HomeController extends Controller {

	private $twig;

	public function __construct() {
		$this->twig = new Environment(new FilesystemLoader( __DIR__ . "/../assets"), []);
		$this->twig->addFunction(new TwigFunction("env", function($variable) {
			return getenv($variable);
		}));
	}

	public function render(): void {
		echo $this->twig->render("index.html");
	}

}
