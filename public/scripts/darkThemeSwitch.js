'use strict';

const bulb = document.querySelector(".l");
const fullChange = document.querySelector('body');
const imgs = document.querySelectorAll('.parallax > img');
const logos = document.querySelectorAll('#organizers img');
const newSrcs = ['/images/idea.jpg', '/images/data3.jpg', '/images/bg-servers3.jpg'];
const oldSrcs = ['/images/clouds.jpg', '/images/data.jpg', '/images/servers.jpg'];
const newLogos = ['/images/logodark.png', '/images/pwszdark.png'];
const oldLogos = ['/images/itmation.png', '/images/pwsz.png'];
bulb.addEventListener("click", (evt) => {
    let className = 'dark-theme';
    
	if (fullChange.classList) {
		fullChange.classList.toggle(className);
	} else {
		var classes = fullChange.className.split(' ');
		var existingIndex = classes.indexOf(className);
	
		if (existingIndex >= 0)
			classes.splice(existingIndex, 1);
		else
			classes.push(className);
	
		fullChange.className = classes.join(' ');
    }
    for(let i = 0; i < imgs.length; i++) {
        if(imgs[i].src === 'http://' + window.location.hostname + newSrcs[i] || imgs[i].src === 'https://' + window.location.hostname + newSrcs[i]) {
            imgs[i].src = oldSrcs[i];
        } else {
            imgs[i].src = newSrcs[i];
        }
    }
	for(let i = 0; i < logos.length; i++) {
        if(logos[i].src === 'http://' + window.location.hostname + newLogos[i] || logos[i].src === 'https://' + window.location.hostname + newLogos[i]) {
            logos[i].src = oldLogos[i];
        } else {
            logos[i].src = newLogos[i];
        }
    }
});