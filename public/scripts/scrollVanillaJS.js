'use strict';

function scroll(emitter, listener, speed) {
    let currentPosition = 0;
    let topPosition = 0;
    let speedModifier = speed;
    let direction = 0;

    // one element - one listener
    if (typeof emitter === 'string' && typeof listener === 'string') {
        if (!emitter) {
            throw 'First argument had not been passed or is invalid';
        } else {
            emitter = document.querySelector(emitter);
            if(emitter == null) {
                throw 'There is not any element with id, class or tag equal to ' + emitter;
            }
        }

        if (!listener) {
            throw 'First argument had not been passed or is invalid';
        } else {
            listener = document.querySelector(listener);
            
            if(listener == null) {
                throw 'There is not any element with id, class or tag equal to ' + listener;
            }
        }

        topPosition = listener.offsetTop;

        emitter.addEventListener('click', function() {
            currentPosition = window.scrollY;

            
            if (topPosition > currentPosition) {
                direction = 1;
            } else if (topPosition < currentPosition) {
                direction = -1;
            }

            let interval = setInterval(function(){
                window.scrollTo(0, currentPosition);
                currentPosition = currentPosition + direction * speedModifier;
                if(currentPosition <= topPosition && direction === -1 || currentPosition >= topPosition && direction === 1) {
                    clearInterval(interval);
                    window.scrollTo(0, topPosition);
                }
            }, 1);
            
        });
        
    }


    // array of elements - one listener - idk if it's useful

    // array of elements - array of listeners
    if (Array.isArray(emitter) && Array.isArray(listener)) {
        topPosition = [];
        
        if(emitter.length > 1) {
            for(let i = 0; i < emitter.length; i++) {
                if (!emitter[i]) {
                    throw 'First argument had not been passed or is invalid';
                } else {
                    emitter[i] = document.querySelector(emitter[i]);
                    
                    if(emitter[i] == null) {
                        throw 'There is not any element with id, class or tag equal to ' + emitter[i];
                    }

                }
            }
        } else {
            if (!emitter[0]) {
                throw 'First argument had not been passed or is invalid';
            } else {
            emitter = document.querySelectorAll(emitter[0]);
            }
        }

        if(listener.length > 1) {
            for(let i = 0; i < listener.length; i++) {
                if (!listener[i]) {
                    throw 'First argument had not been passed or is invalid';
                } else {
                    listener[i] = document.querySelector(listener[i]);
                    
                    if(listener[i] == null) {
                        throw 'There is not any element with id, class or tag equal to ' + listener[i];
                    }

                }
            }
        } else {
            if (!listener[0]) {
                throw 'First argument had not been passed or is invalid';
            } else {
            listener = document.querySelectorAll(listener[0]);
            }
        }

        for(let i = 0; i < emitter.length; i++) {
            topPosition[i] = listener[i].offsetTop;
            
            emitter[i].addEventListener('click', function() {
                currentPosition = window.scrollY;
    
                
                if (topPosition[i] > currentPosition) {
                    direction = 1;
                } else if (topPosition[i] < currentPosition) {
                    direction = -1;
                }
    
                let interval = setInterval(function(){
                    window.scrollTo(0, currentPosition);
                    currentPosition = currentPosition + direction * speedModifier;
                    if(currentPosition <= topPosition[i] && direction === -1 || currentPosition >= topPosition[i] && direction === 1) {
                        clearInterval(interval);
                        window.scrollTo(0, topPosition[i]);
                    }
                }, 1);
                
            });
        }

    }
};

scroll(['.custom-logo', '.first-menu-option', '.second-menu-option', '.third-menu-option', '.fourth-menu-option', '.fifth-menu-option'], ['#landing', '#plan', '#details', '#organization', '#participants ', '#contact' ], 20);
