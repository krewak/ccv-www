'use strict';

const mobileButton = document.querySelector(".menu-mobile");
const mobileLogo = document.querySelector(".custom-logo");
const mobileOptions = document.querySelectorAll(".menu-option");
const mobileScreen = document.querySelector("nav");

mobileButton.addEventListener("click", function(evt) {
	evt.preventDefault();
	
	let className = 'mobile-button-clicked';
	let className2 = 'mobile-container-displayed';
	if (mobileButton.classList) {
		mobileButton.classList.toggle(className);
	} else {
		var classes = mobileButton.className.split(' ');
		var existingIndex = classes.indexOf(className);
	
		if (existingIndex >= 0)
			classes.splice(existingIndex, 1);
		else
			classes.push(className);
	
		mobileButton.className = classes.join(' ');
	}
	if (mobileScreen.classList) {
		mobileScreen.classList.toggle(className2);
	} else {
		var classes = mobileScreen.className.split(' ');
		var existingIndex = classes.indexOf(className2);
	
		if (existingIndex >= 0)
			classes.splice(existingIndex, 1);
		else
			classes.push(className2);
	
		mobileScreen.className = classes.join(' ');
	}
});

mobileLogo.addEventListener("click", function(evt) {
	evt.preventDefault();

	let className = 'mobile-container-displayed';
	let className2 = 'mobile-button-clicked';
	if (mobileScreen.classList) {
		  mobileScreen.classList.remove(className);
		  mobileButton.classList.remove(className2);
	} else {
		  mobileScreen.className = mobileScreen.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
		  mobileButton.className2 = mobileButton.className2.replace(new RegExp('(^|\\b)' + className2.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
	}

});

for(let i = 0; i < mobileOptions.length; i++) {
	mobileOptions[i].addEventListener("click", (evt) => {
		let className = 'mobile-container-displayed';
		let className2 = 'mobile-button-clicked';
		
		if (mobileScreen.classList) {
		  	mobileScreen.classList.remove(className);
		  	mobileButton.classList.remove(className2);
		} else {
		  	mobileScreen.className = mobileScreen.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
		  	mobileButton.className2 = mobileButton.className2.replace(new RegExp('(^|\\b)' + className2.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
		}
	});
}