function initMap() {
	var PWSZLocation = { lat: 51.204469, lng: 16.150093 };

	var map = new google.maps.Map(document.getElementById("map"), {
		zoom: 14,
		center: PWSZLocation
	});

	var marker = new google.maps.Marker({
		position: PWSZLocation,
		map: map,
		icon: "/images/marker.png"
	});
}
