$(document).ready(function() {
	$(".button-collapse").sideNav();
	$(".parallax").parallax();
});

$(document).scroll(function() {
	let landing = $("#landing").height();
	let position = $(this).scrollTop();

	if(position > landing) {
		$("#navigation").addClass("fixed");
	} else {
		$("#navigation").removeClass("fixed");
	}
});