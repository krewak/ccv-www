$(document).ready(function() {
	$(".reservation-checkbox").change(function() {
		let isChecked = this.checked;

		$(".reservation-checkbox").prop("checked", false);
		$(this).prop("checked", isChecked);
	});

	$("#send").click(function() {
		$(".error").removeClass("error");
		$(".error-bag").html("");
		$(this).removeClass("pulse").find("i").addClass("loading").html("loop");

		let request = $.ajax({
			method: "POST",
			data: {
				name: $("#personal-data-input").val(),
				affiliation: $("#affiliation-input").val(),
				email: $("#email-input").val(),
				reservation: $("[name='reservation']:checked").val(),
				additional: $("#additional-information-input").val(),
			}
		});

		request.done(function(response) {
			$("#contact").find("form.scale-transition").addClass("scale-out").height(0);
			$("#contact").find("div.scale-transition.scale-out").addClass("scale-in");
		});

		request.fail(function(response) {
			$("#send").addClass("pulse").find("i").removeClass("loading").html("send");

			let errors = response.responseJSON.errors;
			Object.keys(errors).forEach(function(key, value) {
				$("[data-validation='" + key + "']").addClass("error").find(".error-bag").html(errors[key]);
			});			
		});
	});
});
