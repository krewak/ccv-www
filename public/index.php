<?php

use Dotenv\Dotenv;
use CCV\EmailController;
use CCV\HomeController;

$root = __DIR__ . "/../";

require $root . "vendor/autoload.php";

$dotenv = new Dotenv($root);
$dotenv->load();

$request = $_POST;

if(!empty($request)) {
	$controller = new EmailController($request);
} else {
	$controller = new HomeController();
}

$controller->render();
